declare const Config: {
    notifications: {
        title: string;
        description: string;
        type: string;
        default: boolean;
        get: any;
    };
};
export default Config;
