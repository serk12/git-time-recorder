export declare const current_config: {
    notifications: {
        title: string;
        description: string;
        type: string;
        default: boolean;
        get: any;
    };
};
export declare function activate(): void;
