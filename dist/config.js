"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var prefix = "git-time-recorder.";
function getValue(key) {
    return atom.config.get(prefix + key);
}
const Config = {
    notifications: {
        title: "Show notification",
        description: "show a blue notification when starting running and when it's completed",
        type: "boolean",
        default: true,
        get: getValue.bind(null, "notifications"),
    },
};
exports.default = Config;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29uZmlnLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vbGliL2NvbmZpZy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxZQUFZLENBQUM7O0FBRWIsSUFBSSxNQUFNLEdBQUcsb0JBQW9CLENBQUM7QUFDbEMsU0FBUyxRQUFRLENBQUMsR0FBVztJQUMzQixPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLE1BQU0sR0FBRyxHQUFHLENBQUMsQ0FBQztBQUN2QyxDQUFDO0FBRUQsTUFBTSxNQUFNLEdBQUc7SUFDYixhQUFhLEVBQUU7UUFDYixLQUFLLEVBQUUsbUJBQW1CO1FBQzFCLFdBQVcsRUFDVCx3RUFBd0U7UUFDMUUsSUFBSSxFQUFFLFNBQVM7UUFDZixPQUFPLEVBQUUsSUFBSTtRQUNiLEdBQUcsRUFBRSxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxlQUFlLENBQUM7S0FDMUM7Q0FDRixDQUFDO0FBRUYsa0JBQWUsTUFBTSxDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiXCJ1c2Ugc3RyaWN0XCI7XG5cbnZhciBwcmVmaXggPSBcImdpdC10aW1lLXJlY29yZGVyLlwiO1xuZnVuY3Rpb24gZ2V0VmFsdWUoa2V5OiBzdHJpbmcpOiB2b2lkIHtcbiAgcmV0dXJuIGF0b20uY29uZmlnLmdldChwcmVmaXggKyBrZXkpO1xufVxuXG5jb25zdCBDb25maWcgPSB7XG4gIG5vdGlmaWNhdGlvbnM6IHtcbiAgICB0aXRsZTogXCJTaG93IG5vdGlmaWNhdGlvblwiLFxuICAgIGRlc2NyaXB0aW9uOlxuICAgICAgXCJzaG93IGEgYmx1ZSBub3RpZmljYXRpb24gd2hlbiBzdGFydGluZyBydW5uaW5nIGFuZCB3aGVuIGl0J3MgY29tcGxldGVkXCIsXG4gICAgdHlwZTogXCJib29sZWFuXCIsXG4gICAgZGVmYXVsdDogdHJ1ZSxcbiAgICBnZXQ6IGdldFZhbHVlLmJpbmQobnVsbCwgXCJub3RpZmljYXRpb25zXCIpLFxuICB9LFxufTtcblxuZXhwb3J0IGRlZmF1bHQgQ29uZmlnO1xuIl19