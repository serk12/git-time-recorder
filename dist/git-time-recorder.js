"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.activate = exports.current_config = void 0;
const atom_1 = require("atom");
const config_js_1 = __importDefault(require("./config.js"));
const fs_1 = __importDefault(require("fs"));
const git_last_commit_1 = __importDefault(require("git-last-commit"));
exports.current_config = config_js_1.default;
function logNotification(text, tittle = "-- DEBUG --") {
    const args = { dismissable: tittle === "-- DEBUG --", detail: text };
    atom.notifications.addInfo(tittle, args);
}
const record = new Set();
function getCurrentFilePath() {
    return atom.workspace.getActivePaneItem().getPath();
}
function clock(file) {
    if (file === undefined) {
        file = getCurrentFilePath();
    }
    atom.project
        .repositoryForDirectory(new atom_1.Directory(file, false))
        .then((repo) => {
        const root_repo = repo.getWorkingDirectory();
        git_last_commit_1.default.getLastCommit((err, commit) => {
            if (err) {
                logNotification("error", err.message);
            }
            else {
                const clocking_text = new Date().toLocaleDateString().toString() +
                    " " +
                    commit.branch +
                    " " +
                    commit.author.name +
                    " " +
                    commit.subject +
                    "\n";
                if (!record.has(clocking_text)) {
                    fs_1.default.appendFile(root_repo + "/.git_time_recorder", clocking_text, null, (error) => {
                        if (error) {
                            logNotification(error.message);
                        }
                    });
                    record.add(clocking_text);
                }
            }
        }, { dst: root_repo });
    });
}
function activate() {
    atom.commands.add("atom-workspace", {
        "git-time-recorder:clock": () => {
            clock();
        },
    });
    atom.workspace.observeTextEditors((editor) => {
        editor.onDidSave(() => {
            atom.notifications.clear();
            const files = getCurrentFilePath();
            if (files.length === 0)
                return;
            logNotification("", "Running git-time-recorder");
            clock();
        });
    });
}
exports.activate = activate;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ2l0LXRpbWUtcmVjb3JkZXIuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi9saWIvZ2l0LXRpbWUtcmVjb3JkZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsWUFBWSxDQUFDOzs7Ozs7QUFFYiwrQkFBaUM7QUFDakMsNERBQWlDO0FBQ2pDLDRDQUFvQjtBQUNwQixzRUFBa0M7QUFFckIsUUFBQSxjQUFjLEdBQUcsbUJBQU0sQ0FBQztBQUVyQyxTQUFTLGVBQWUsQ0FBQyxJQUFZLEVBQUUsTUFBTSxHQUFHLGFBQWE7SUFDM0QsTUFBTSxJQUFJLEdBQUcsRUFBRSxXQUFXLEVBQUUsTUFBTSxLQUFLLGFBQWEsRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFFLENBQUM7SUFDckUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxDQUFDO0FBQzNDLENBQUM7QUFFRCxNQUFNLE1BQU0sR0FBRyxJQUFJLEdBQUcsRUFBVSxDQUFDO0FBRWpDLFNBQVMsa0JBQWtCO0lBRXpCLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQyxpQkFBaUIsRUFBRSxDQUFDLE9BQU8sRUFBRSxDQUFDO0FBQ3RELENBQUM7QUFFRCxTQUFTLEtBQUssQ0FBQyxJQUFhO0lBQzFCLElBQUksSUFBSSxLQUFLLFNBQVMsRUFBRSxDQUFDO1FBQ3ZCLElBQUksR0FBRyxrQkFBa0IsRUFBRSxDQUFDO0lBQzlCLENBQUM7SUFDRCxJQUFJLENBQUMsT0FBTztTQUNULHNCQUFzQixDQUFDLElBQUksZ0JBQVMsQ0FBQyxJQUFJLEVBQUUsS0FBSyxDQUFDLENBQUM7U0FDbEQsSUFBSSxDQUFDLENBQUMsSUFBSSxFQUFFLEVBQUU7UUFFYixNQUFNLFNBQVMsR0FBRyxJQUFJLENBQUMsbUJBQW1CLEVBQUUsQ0FBQztRQUM3Qyx5QkFBRyxDQUFDLGFBQWEsQ0FDZixDQUFDLEdBQUcsRUFBRSxNQUFNLEVBQUUsRUFBRTtZQUNkLElBQUksR0FBRyxFQUFFLENBQUM7Z0JBQ1IsZUFBZSxDQUFDLE9BQU8sRUFBRSxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDeEMsQ0FBQztpQkFBTSxDQUFDO2dCQUNOLE1BQU0sYUFBYSxHQUNqQixJQUFJLElBQUksRUFBRSxDQUFDLGtCQUFrQixFQUFFLENBQUMsUUFBUSxFQUFFO29CQUMxQyxHQUFHO29CQUNILE1BQU0sQ0FBQyxNQUFNO29CQUNiLEdBQUc7b0JBQ0gsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJO29CQUNsQixHQUFHO29CQUNILE1BQU0sQ0FBQyxPQUFPO29CQUNkLElBQUksQ0FBQztnQkFDUCxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxhQUFhLENBQUMsRUFBRSxDQUFDO29CQUMvQixZQUFFLENBQUMsVUFBVSxDQUNYLFNBQVMsR0FBRyxxQkFBcUIsRUFDakMsYUFBYSxFQUNiLElBQUksRUFDSixDQUFDLEtBQUssRUFBRSxFQUFFO3dCQUNSLElBQUksS0FBSyxFQUFFLENBQUM7NEJBQ1YsZUFBZSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQzt3QkFDakMsQ0FBQztvQkFDSCxDQUFDLENBQ0YsQ0FBQztvQkFDRixNQUFNLENBQUMsR0FBRyxDQUFDLGFBQWEsQ0FBQyxDQUFDO2dCQUM1QixDQUFDO1lBQ0gsQ0FBQztRQUNILENBQUMsRUFDRCxFQUFFLEdBQUcsRUFBRSxTQUFTLEVBQUUsQ0FDbkIsQ0FBQztJQUNKLENBQUMsQ0FBQyxDQUFDO0FBQ1AsQ0FBQztBQUVELFNBQWdCLFFBQVE7SUFDdEIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLEVBQUU7UUFDbEMseUJBQXlCLEVBQUUsR0FBRyxFQUFFO1lBQzlCLEtBQUssRUFBRSxDQUFDO1FBQ1YsQ0FBQztLQUNGLENBQUMsQ0FBQztJQUNILElBQUksQ0FBQyxTQUFTLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxNQUFNLEVBQUUsRUFBRTtRQUMzQyxNQUFNLENBQUMsU0FBUyxDQUFDLEdBQUcsRUFBRTtZQUNwQixJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssRUFBRSxDQUFDO1lBQzNCLE1BQU0sS0FBSyxHQUFHLGtCQUFrQixFQUFFLENBQUM7WUFDbkMsSUFBSSxLQUFLLENBQUMsTUFBTSxLQUFLLENBQUM7Z0JBQUUsT0FBTztZQUMvQixlQUFlLENBQUMsRUFBRSxFQUFFLDJCQUEyQixDQUFDLENBQUM7WUFDakQsS0FBSyxFQUFFLENBQUM7UUFDVixDQUFDLENBQUMsQ0FBQztJQUNMLENBQUMsQ0FBQyxDQUFDO0FBQ0wsQ0FBQztBQWZELDRCQWVDIiwic291cmNlc0NvbnRlbnQiOlsiXCJ1c2Ugc3RyaWN0XCI7XG5cbmltcG9ydCB7IERpcmVjdG9yeSB9IGZyb20gXCJhdG9tXCI7XG5pbXBvcnQgQ29uZmlnIGZyb20gXCIuL2NvbmZpZy5qc1wiO1xuaW1wb3J0IGZzIGZyb20gXCJmc1wiO1xuaW1wb3J0IGdpdCBmcm9tIFwiZ2l0LWxhc3QtY29tbWl0XCI7XG5cbmV4cG9ydCBjb25zdCBjdXJyZW50X2NvbmZpZyA9IENvbmZpZztcblxuZnVuY3Rpb24gbG9nTm90aWZpY2F0aW9uKHRleHQ6IHN0cmluZywgdGl0dGxlID0gXCItLSBERUJVRyAtLVwiKTogdm9pZCB7XG4gIGNvbnN0IGFyZ3MgPSB7IGRpc21pc3NhYmxlOiB0aXR0bGUgPT09IFwiLS0gREVCVUcgLS1cIiwgZGV0YWlsOiB0ZXh0IH07XG4gIGF0b20ubm90aWZpY2F0aW9ucy5hZGRJbmZvKHRpdHRsZSwgYXJncyk7XG59XG5cbmNvbnN0IHJlY29yZCA9IG5ldyBTZXQ8c3RyaW5nPigpO1xuXG5mdW5jdGlvbiBnZXRDdXJyZW50RmlsZVBhdGgoKSB7XG4gIC8vIEB0cy1leHBlY3QtZXJyb3JcbiAgcmV0dXJuIGF0b20ud29ya3NwYWNlLmdldEFjdGl2ZVBhbmVJdGVtKCkuZ2V0UGF0aCgpO1xufVxuXG5mdW5jdGlvbiBjbG9jayhmaWxlPzogc3RyaW5nKSB7XG4gIGlmIChmaWxlID09PSB1bmRlZmluZWQpIHtcbiAgICBmaWxlID0gZ2V0Q3VycmVudEZpbGVQYXRoKCk7XG4gIH1cbiAgYXRvbS5wcm9qZWN0XG4gICAgLnJlcG9zaXRvcnlGb3JEaXJlY3RvcnkobmV3IERpcmVjdG9yeShmaWxlLCBmYWxzZSkpXG4gICAgLnRoZW4oKHJlcG8pID0+IHtcbiAgICAgIC8vIGNvbnN0IGJyYW5jaCA9IHJlcG8uZ2V0VXBzdHJlYW1CcmFuY2goZmlsZSk7XG4gICAgICBjb25zdCByb290X3JlcG8gPSByZXBvLmdldFdvcmtpbmdEaXJlY3RvcnkoKTtcbiAgICAgIGdpdC5nZXRMYXN0Q29tbWl0KFxuICAgICAgICAoZXJyLCBjb21taXQpID0+IHtcbiAgICAgICAgICBpZiAoZXJyKSB7XG4gICAgICAgICAgICBsb2dOb3RpZmljYXRpb24oXCJlcnJvclwiLCBlcnIubWVzc2FnZSk7XG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGNvbnN0IGNsb2NraW5nX3RleHQgPVxuICAgICAgICAgICAgICBuZXcgRGF0ZSgpLnRvTG9jYWxlRGF0ZVN0cmluZygpLnRvU3RyaW5nKCkgK1xuICAgICAgICAgICAgICBcIiBcIiArXG4gICAgICAgICAgICAgIGNvbW1pdC5icmFuY2ggK1xuICAgICAgICAgICAgICBcIiBcIiArXG4gICAgICAgICAgICAgIGNvbW1pdC5hdXRob3IubmFtZSArXG4gICAgICAgICAgICAgIFwiIFwiICtcbiAgICAgICAgICAgICAgY29tbWl0LnN1YmplY3QgK1xuICAgICAgICAgICAgICBcIlxcblwiO1xuICAgICAgICAgICAgaWYgKCFyZWNvcmQuaGFzKGNsb2NraW5nX3RleHQpKSB7XG4gICAgICAgICAgICAgIGZzLmFwcGVuZEZpbGUoXG4gICAgICAgICAgICAgICAgcm9vdF9yZXBvICsgXCIvLmdpdF90aW1lX3JlY29yZGVyXCIsXG4gICAgICAgICAgICAgICAgY2xvY2tpbmdfdGV4dCxcbiAgICAgICAgICAgICAgICBudWxsLFxuICAgICAgICAgICAgICAgIChlcnJvcikgPT4ge1xuICAgICAgICAgICAgICAgICAgaWYgKGVycm9yKSB7XG4gICAgICAgICAgICAgICAgICAgIGxvZ05vdGlmaWNhdGlvbihlcnJvci5tZXNzYWdlKTtcbiAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICApO1xuICAgICAgICAgICAgICByZWNvcmQuYWRkKGNsb2NraW5nX3RleHQpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgeyBkc3Q6IHJvb3RfcmVwbyB9LFxuICAgICAgKTtcbiAgICB9KTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGFjdGl2YXRlKCkge1xuICBhdG9tLmNvbW1hbmRzLmFkZChcImF0b20td29ya3NwYWNlXCIsIHtcbiAgICBcImdpdC10aW1lLXJlY29yZGVyOmNsb2NrXCI6ICgpID0+IHtcbiAgICAgIGNsb2NrKCk7XG4gICAgfSxcbiAgfSk7XG4gIGF0b20ud29ya3NwYWNlLm9ic2VydmVUZXh0RWRpdG9ycygoZWRpdG9yKSA9PiB7XG4gICAgZWRpdG9yLm9uRGlkU2F2ZSgoKSA9PiB7XG4gICAgICBhdG9tLm5vdGlmaWNhdGlvbnMuY2xlYXIoKTtcbiAgICAgIGNvbnN0IGZpbGVzID0gZ2V0Q3VycmVudEZpbGVQYXRoKCk7XG4gICAgICBpZiAoZmlsZXMubGVuZ3RoID09PSAwKSByZXR1cm47XG4gICAgICBsb2dOb3RpZmljYXRpb24oXCJcIiwgXCJSdW5uaW5nIGdpdC10aW1lLXJlY29yZGVyXCIpO1xuICAgICAgY2xvY2soKTtcbiAgICB9KTtcbiAgfSk7XG59XG4iXX0=