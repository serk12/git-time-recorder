"use strict";

var prefix = "git-time-recorder.";
function getValue(key: string): void {
  return atom.config.get(prefix + key);
}

const Config = {
  notifications: {
    title: "Show notification",
    description:
      "show a blue notification when starting running and when it's completed",
    type: "boolean",
    default: true,
    get: getValue.bind(null, "notifications"),
  },
};

export default Config;
