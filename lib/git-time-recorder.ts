"use strict";

import { Directory } from "atom";
import Config from "./config.js";
import fs from "fs";
import git from "git-last-commit";

export const current_config = Config;

function logNotification(text: string, tittle = "-- DEBUG --"): void {
  const args = { dismissable: tittle === "-- DEBUG --", detail: text };
  atom.notifications.addInfo(tittle, args);
}

const record = new Set<string>();

function getCurrentFilePath() {
  // @ts-expect-error
  return atom.workspace.getActivePaneItem().getPath();
}

function clock(file?: string) {
  if (file === undefined) {
    file = getCurrentFilePath();
  }
  atom.project
    .repositoryForDirectory(new Directory(file, false))
    .then((repo) => {
      // const branch = repo.getUpstreamBranch(file);
      const root_repo = repo.getWorkingDirectory();
      git.getLastCommit(
        (err, commit) => {
          if (err) {
            logNotification("error", err.message);
          } else {
            const clocking_text =
              new Date().toLocaleDateString().toString() +
              " " +
              commit.branch +
              " " +
              commit.author.name +
              " " +
              commit.subject +
              "\n";
            if (!record.has(clocking_text)) {
              fs.appendFile(
                root_repo + "/.git_time_recorder",
                clocking_text,
                null,
                (error) => {
                  if (error) {
                    logNotification(error.message);
                  }
                },
              );
              record.add(clocking_text);
            }
          }
        },
        { dst: root_repo },
      );
    });
}

export function activate() {
  atom.commands.add("atom-workspace", {
    "git-time-recorder:clock": () => {
      clock();
    },
  });
  atom.workspace.observeTextEditors((editor) => {
    editor.onDidSave(() => {
      atom.notifications.clear();
      const files = getCurrentFilePath();
      if (files.length === 0) return;
      logNotification("", "Running git-time-recorder");
      clock();
    });
  });
}
